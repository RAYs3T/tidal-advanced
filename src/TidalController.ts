import {SongInformation} from "./entity/SongInformation";
import {TidalSelectors} from "./entity/TidalSelectors";

export class TidalController {


    static DEBUG_PREFIX = "[TA]";

    selectors = new TidalSelectors();

    constructor() {
        this.selectors.songTitle = "[data-test-id=footer-track-title]";
        this.selectors.songArtist = "[data-test-id=grid-item-detail-text-title-artist]";
    }

    public getCurrentSong(): SongInformation {
        this.debug("Reading song information ...");
        let songInformation = new SongInformation();
        let titleSelector = document.querySelector(this.selectors.songTitle);
        let artistSelector = document.querySelector(this.selectors.songArtist);

        songInformation.title = titleSelector === null ? null : titleSelector.textContent;
        songInformation.artist = artistSelector === null ? null : artistSelector.textContent;
        return songInformation;
    }

    public loadDynamicRangeDataForSong(song: SongInformation) {
        // TODO
    }

    private debug(message: string, obj?: object) {
        if (obj == null) {
            console.debug(TidalController.DEBUG_PREFIX + " " + message);
        } else {
            console.debug(TidalController.DEBUG_PREFIX + " " + message, obj);
        }

    }
}