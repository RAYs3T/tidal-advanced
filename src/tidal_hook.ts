import {TidalController} from "./TidalController";

console.log("Loading TIDAL Advanced");

const controller = new TidalController();


setTimeout(function () {
    let song = controller.getCurrentSong();
    console.log();
    controller.loadDynamicRangeDataForSong(song);
}, 2000);
